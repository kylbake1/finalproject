﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PauseMenu : MonoBehaviour
{
    public string levelSelect; // creates a slot to type in the scene name
    public string mainMenu; // creates a slot to a type in the main menu name
    public bool isPaused; // creates a bool if we paused the game
    public GameObject pauseMenuCanvas; // creates a slot to attach the gameobject pause menu

    void Update()
    {
        if (isPaused)
        {
            pauseMenuCanvas.SetActive(true);// if we pause the game
            Time.timeScale = 0; // freeze everything in place
        }
        else
        {
            pauseMenuCanvas.SetActive(false); // if we unpause
            Time.timeScale = 1; // unfreeze everything
        }

        if (Input.GetKeyDown(KeyCode.P)) // user input for P key
        {
            isPaused = !isPaused; // game is pause/ game is unpaused
        }
    }

    public void Resume()
    {
        isPaused = false; // resume the game if ispaused is false
    }

    public void LevelSelect()
    {
        SceneManager.LoadScene(levelSelect); // loads the scene levelSelect
    }

    public void Quit()
    {
        SceneManager.LoadScene(mainMenu); // loads the scene mainMenu
    }
}
