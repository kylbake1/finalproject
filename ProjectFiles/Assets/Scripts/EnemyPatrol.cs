﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{

    public float moveSpeed; // public float called movespeed for enemy
    public bool moveRight; // bool to keep track of left and right movements

    public Transform wallCheck; // check to see if there is a wall in the way
    public float wallCheckRadius; // distance between enemy and wall
    public LayerMask whatIsWall; // layermask called whatiswall that we let in layer
    private bool isHittingWall; // bool to know if enemy hits a wall

    private bool notAtEdge; // bool to check if enemy is at the edge
    public Transform edgeCheck; // check if there is a ground block

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        isHittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall); //  this will help determine if the enemy is hitting the wall with all the player set values in the inspector

        notAtEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall); // this will help determine if the enemy is at an edge and not run off

        if (isHittingWall || !notAtEdge) // if the enemy is hitting a wall and is at an edge
            moveRight = !moveRight; // turn to the right or left

        if (moveRight) // this will change the x scale to allow the enemy to change directions
        {
            transform.localScale = new Vector3(-1f, 1f, 1f); // change xscale value to -1 to go right
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y); // changes velocity in current direction
        }
        else
        {
            transform.localScale = new Vector3(1f, 1f, 1f); // changes the xscale to 1 and enemy goes left
            GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);// changes velocity in current direction
        }
    }
}
