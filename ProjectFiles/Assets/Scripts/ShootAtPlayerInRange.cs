﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootAtPlayerInRange : MonoBehaviour
{
    public float playerRange; // playerRange can be set by user
    public GameObject enemyStar;// slot created for enemy ninja star
    public PlayerController player;// player controller slot for player
    public Transform launchPoint;// launchpoint gameobject for enemy shooting
    public float waitBetweenShots;// cooldown for no rapid shooting
    private float shotCounter;// keeps track of shots

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();//player is automatically filled with playercontroller script
        shotCounter = waitBetweenShots;//shotcounter is set equal to cool down
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawLine (new Vector3(transform.position.x - playerRange, transform.position.y, transform.position.z) , new Vector3(transform.position.x + playerRange, transform.position.y, transform.position.z)); //shows distance enemy can see if crossed they will shoot
        shotCounter -= Time.deltaTime; // cool down is reduced

        if (transform.localScale.x < 0 && player.transform.position.x > transform.position.x && player.transform.position.x < transform.position.x+playerRange && shotCounter < 0)  // if his local scale is -1 than we know he's moving right
        {
            Instantiate(enemyStar, launchPoint.position, launchPoint.rotation); // clone the enemystar when shooting
            shotCounter = waitBetweenShots;// cooldown starts
        }
        if (transform.localScale.x > 0 && player.transform.position.x < transform.position.x && player.transform.position.x > transform.position.x - playerRange && shotCounter < 0)  // if his local scale is -1 than we know he's moving right
        {
            Instantiate(enemyStar, launchPoint.position, launchPoint.rotation); // clone the enemystar when shooting
            shotCounter = waitBetweenShots;// cooldown starts
        }
    }
}
