﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoints : MonoBehaviour
{
    public LevelManager levelManager; // creates empty LevelManager of type call it levelManager 


    // Start is called before the first frame update
    void Start()
    {
        levelManager = FindObjectOfType<LevelManager>(); // gets levelManager and finds any object that has LevelManager attached to it
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision) //  if collider is triggered
    {
        if (collision.name == "Player") // the player can trigger the collider
        {
            levelManager.currentCheckpoints = gameObject; // the checkpoint the player triggered is now active for respawn point
            Debug.Log("Activated Checkpoint :" + transform.position); // display the checkpoint active
        }
    }
}
