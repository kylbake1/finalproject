﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    public LevelManager levelManager; // creates empty LevelManager of type call it levelManager 


    // Start is called before the first frame update
    void Start()
    {
        levelManager = FindObjectOfType<LevelManager>(); // gets levelManager and finds any object that has LevelManager attached to it
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision) // if something enters the player collider
    {
        if(collision.name == "Player") // collider is called player
        {
            levelManager.RespawnPlayer(); // player dies and player is respawned 
        }
    }
}
