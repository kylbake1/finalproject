﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject currentCheckpoints; // creates a slot for the current checkpoint weve activated
    private PlayerController player; // playercontroller is called player
    public GameObject deathParticle; // creates a slot for the death particle
    public GameObject respawnParticle;// creates a slot for the respawn particle
    public float respawnDelay; // delays our respawn time so its not instant and allows player to process
    private new CameraController camera;// cameracontroller is now called camera
    public int pointsPenaltyOnDeath;// lose points on death
    private float gravityStore;// gravity value 
    public HealthManager healthManager;// healthmanager script is called healthmanager

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>(); // automatically set the player to playercontroller on start
        camera = FindObjectOfType<CameraController>();//automatically sets the camera to cameracontroller on start
        healthManager = FindObjectOfType<HealthManager>();// automatically sets the healthmanager to healthmanager on start
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RespawnPlayer()
    {
        StartCoroutine("RespawnPlayerCo");
    }

    public IEnumerator RespawnPlayerCo()
    {
        Debug.Log("Dead"); //  displays dead in console message
        Instantiate(deathParticle, player.transform.position, player.transform.rotation); // clones the death particles in player position
        player.enabled = false;// player is no longer visiable
        player.GetComponent<Renderer>().enabled = false; // player rendere is off
        gravityStore = player.GetComponent<Rigidbody2D>().gravityScale; // gravity is set to current gravity
        player.GetComponent<Rigidbody2D>().gravityScale = 0f; // gravity is now 0
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero; // velocity is set to 0
        camera.isFollowing = false;// camera is no longer following player
        ScoreManager.AddPoints(-pointsPenaltyOnDeath);// points are deducted 

        Debug.Log("Rrrrrrespawn!");
        yield return new WaitForSeconds(respawnDelay); // player waits set number of time to respawn
        player.GetComponent<Rigidbody2D>().gravityScale = gravityStore; // gravity store is now restored to set value in inspector
        player.transform.position = currentCheckpoints.transform.position; // point of repostition respawn
        player.knockBackCount = 0; //player doesnt knockback when killed
        player.enabled = true;// player turn back on
        player.GetComponent<Renderer>().enabled = true;// can see player again
        healthManager.fullHealth();// health is restored to full
        healthManager.isDead = false;// player is no longer dead and wont start death particle
        camera.isFollowing = true;// camera will now follow player
        Instantiate(respawnParticle, currentCheckpoints.transform.position, currentCheckpoints.transform.rotation); // clone respawn particles in player new position 

    }
}
