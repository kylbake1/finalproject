﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectOverTime : MonoBehaviour
{
    public float lifeTime; // duration of ninja stars

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        lifeTime -= Time.deltaTime; // reduce the time relative to game time
        
        if(lifeTime < 0) // if the lifetime is less than 0
        {
            Destroy(gameObject); // destroy the ninjastar
        }
    }
}
