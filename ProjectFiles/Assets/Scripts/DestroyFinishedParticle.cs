﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyFinishedParticle : MonoBehaviour
{
    private ParticleSystem thisParticleSystem;// this will set the ParticleSystem in the inspector to thisParticleSystem in the script

    // Start is called before the first frame update
    void Start()
    {
        thisParticleSystem = GetComponent<ParticleSystem>(); // automatically sets the particle system to thisparticlesystem
    }

    // Update is called once per frame
    void Update()
    {
        if (thisParticleSystem.isPlaying) // if the particle system is playing
            return;//keep playing
        Destroy(gameObject);// else destroy the particle system
    }

    void OnBecameInvisible() // when it becomes invisible to game
    {
        Destroy(gameObject); // destroy it, used for enemy and player death splats
    }
}
