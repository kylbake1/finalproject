﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPatrol : MonoBehaviour
{
    public float moveSpeed; // creates a public variable float called moveSpeed
    public bool moveRight; // creates a public bool variable called moveRight

    public Transform wallCheck; // creates a public Transform varibale called wallCheck
    public float wallCheckRadius; // creates a public float variable called wallCheckRadius
    public LayerMask whatIsWall; // creates a public LayerMask called whatisWall
    private bool isHittingWall; // creates a private bool variable called isHittingWall

    private bool notAtEdge; // creates a private bool called notAtEdge
    public Transform edgeCheck; // creates a public Transform variable called edgeCheck
    private Rigidbody2D rb2d; // creates a private rigidbody 2d called rb2d

    private float ySize; // creates a private float called ySize for vertial

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>(); // attaches the rigidbody2d to rb2d
        ySize = transform.localScale.y; // ysize is set equal to the localscale of y when it changes
    }

    // Update is called once per frame
    void Update()
    {
        isHittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall); // is bool is set equal to physics2D.OverLapCircle that will check to see the wall, if the wall is within it's set radius, and if it is a wall or not from tag

        notAtEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall); // bool is set equal to physics2D overlapcircle and this will check if enemy is at an edge by gameobject, then chack if within radius, then see if there is a wall or edge

        if (isHittingWall || !notAtEdge) // if enemy is hitting a wall or not on an edge
            moveRight = !moveRight; // will move to the left otherwise, move right

        if (moveRight) // this will change the direction
        {
            transform.localScale = new Vector3(-ySize, transform.localScale.y, transform.localScale.z); // this will set the scale of the ysize into a positive 1 to move right and leave others the same as current
            rb2d.velocity = new Vector2(moveSpeed, rb2d.velocity.y); // this will adjust the speed in the direction using the rigidbody2d velocity 
        }
        else
        {
            transform.localScale = new Vector3(ySize, transform.localScale.y, transform.localScale.z);// this will set the scale of the ysize into a positive -1 to move right and leave others the same as current
            rb2d.velocity = new Vector2(-moveSpeed, rb2d.velocity.y); // this will adjust the speed in the direction using the rigidbody2d velocity 
        }
    }
}
