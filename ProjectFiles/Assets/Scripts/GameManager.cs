﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Singleton
    public static GameManager instance; // the static variable allows us to use anything in this class
          
    void Awake() // as soon as the game starts
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject); // nothing in here will get destroyed when changing scenes
        }
        else
        {
            Destroy(gameObject); // everything else will
        }
    }
}
