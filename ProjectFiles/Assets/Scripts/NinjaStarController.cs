﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaStarController : MonoBehaviour
{
    public float speed; // creates a float called speed for the ninja star 
    public PlayerController player;// player is called playercontroller
    public GameObject enemyDeathEffect; // creates a slot for enemydeath effect when enemy dies
    public GameObject impactEffect; // creates a slot for impacteffect when we miss and hit a block
    public int pointsForKill;//points when enemy is killed
    public float rotationSpeed;// creates a little animation for star to spin
    public int damageToGive; // how much damage stars give

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>(); // automatically attaches playercontoller to player

        if (player.transform.localScale.x < 0) // if player looks left
        {
            speed = -speed; // send start left or right
            rotationSpeed = -rotationSpeed; //  spin the star 360 or -360
        }
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y); // creates velocity for star
        GetComponent<Rigidbody2D>().angularVelocity = rotationSpeed; // spins the star
    }

    private void OnTriggerEnter2D(Collider2D collision) // if the star triggers
    {
        if(collision.tag == "Enemy") // the enemy
        {

            collision.GetComponent<EnemyHealthManager>().giveDamage(damageToGive); // take away health from enemy from star damage
        }
        if(collision.tag == "Boss") // the boss
        {
            collision.GetComponent<TheBossHealthManager>().giveDamage(damageToGive);// take away health from enemy from star damage
        }
        Instantiate(impactEffect, transform.position, transform.rotation); // creates impact effect close in postion of impact
        Destroy(gameObject); // destroys the star and effect
    }
}
