﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public string startLevel; // this creates a slot in inspector called startLevel for scene
    public string levelSelect;// this creates a slot in inspector called levelSelect for scene
    public int playerLives;// this creates a slot in inspector called playerLives that we can adjust the lives before game begins 
    public int playerHealth;// this creates a slot in inspector called playerHealth that we can adjust before game begins 
    public string levelOneTag;// this creates a slot in inspector called to allow us to unlock doors

    public void NewGame()
    {
        PlayerPrefs.SetInt("PlayerCurrentLives", playerLives); // this will allow us to keep information throughout the game that will set playercurrent lives to our playerlives
        PlayerPrefs.SetInt("CurrentScore", 0); // this will set the score to 0 and allow us to save score throughout game
        PlayerPrefs.SetInt("PlayerCurrentHealth", playerHealth);// this will set the playerhealth and save it throughout game 
        PlayerPrefs.SetInt("PlayerMaxHealth", playerHealth); // this will set the maximum health so we dont exceed the playerhealth 
        PlayerPrefs.SetInt(levelOneTag, 1);// this will allow us to unlock level 1
        
        SceneManager.LoadScene(startLevel); // this will load the scene called startLevel
        

    }

    public void LevelSelect()
    {
        PlayerPrefs.SetInt("PlayerCurrentLives", playerLives); // this will set the playerlives to user choice and save it throughout game in levelSelect
        PlayerPrefs.SetInt("CurrentScore", 0);  // this will set the current score to 0 or to user choice and save it throughout game in levelSelect
        PlayerPrefs.SetInt("PlayerCurrentHealth", playerHealth); // this will set the players current health to user choice and save it throughout game in levelSelect
        PlayerPrefs.SetInt("PlayerMaxHealth", playerHealth); // this will set the player max health so we done exceed the fixed number of lives to user choice and save it throughout game in levelSelect
        PlayerPrefs.SetInt(levelOneTag, 1); // this will unlock the first level in this scene
        SceneManager.LoadScene(levelSelect); // this will load the scene called levelSelect
    }
    
    public void QuitGame()
    {
        Application.Quit(); // ends the game and closes the window
    }
}
