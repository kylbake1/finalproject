﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWall : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (FindObjectOfType<TheBossHealthManager>()) // we find all gameobkect that are attached with the TheBossHealthManager script
        {
            return; // return nothing if boss is still alive with the type
        }
        Destroy(gameObject); // if it is no longer available, destroy the gameobject
    }
}
