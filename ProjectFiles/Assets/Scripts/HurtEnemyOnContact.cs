﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemyOnContact : MonoBehaviour
{
    public int damageToGive; // creates damage points to give to enemy
    public float bounceOnEnemy; // value of how much we will bounce
    private Rigidbody2D rb2d; // rigidbody2d is called rb2d
    

    // Start is called before the first frame update
    void Start()
    {
        rb2d = transform.parent.GetComponent<Rigidbody2D>(); // automatically set script attached rigidbody to rb2d
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collision) // if the collider is triggered
    {
        if (collision.tag == "Enemy") // only triggered by enemy
        {
            collision.GetComponent<EnemyHealthManager>().giveDamage(damageToGive); // reduces the enemy health by damaged dealt by the player
            rb2d.velocity = new Vector2(rb2d.velocity.x, bounceOnEnemy); // toss the player back when we touch it
        }
    }
}
