﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBlockContact : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D collision) // collider is triggered
    { 
        if (collision.tag == "Ground") // only effect the ground
        {

            Destroy(collision.gameObject); // destroy the ground when its triggered by stars

        }
    }
}
