﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    private bool playerInZone; // creates a private bool called playerinzone
    public string levelToLoad; // creates a slot in the inspector that we can enter for leveltoload
    public string levelTag;// creates a slot in the inspector that we can enter for levelTag

    // Start is called before the first frame update
    void Start()
    {
        playerInZone = false; // player in zone is set to false at start of the game
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Vertical") > 0 && playerInZone) // if the user presses W S ^ keys and is in the door 
        {
            LoadLevel(); //this will load the level with the leveltoload scene name 
        }
    }

    public void LoadLevel()
    {
        PlayerPrefs.SetInt(levelTag, 1); // this will load the next level with tag 1 for unlock
        SceneManager.LoadScene(levelToLoad);// this will load the next scene
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player") // if the player triggers the door collider
        {
            playerInZone = true; // the playerinzone is set to true
        }
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Player") // if the player with name leaves the door collider
        {
            playerInZone = false; // the playerinzone is set to false
        }
    }

}
