﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheBossHealthManager : MonoBehaviour
{
    public int enemyHealth; // gives the enemy health so it can die and adjustable in inspector
    public GameObject deathEffect; // allows us to attach deatheffect in the inspector
    public int pointsOnDeath; // points earned
    public GameObject bossPrefab;// creates a gameobject for minis
    public float minSize; // creates the number of minis

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (enemyHealth <= 0)//if the health is less than of equal to 0
        {
            Instantiate(deathEffect, transform.position, transform.rotation); // clone a deatheffect in parent postion/rotation
            ScoreManager.AddPoints(pointsOnDeath); // give player points
            if(transform.localScale.y > minSize) // if the boss size is bigger than set minisize
            {
                GameObject clone1 = Instantiate(bossPrefab, new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z),transform.rotation)as GameObject; // make a clone 1/2 a unit to the left in boss death
                GameObject clone2 = Instantiate(bossPrefab, new Vector3(transform.position.x - .5f, transform.position.y, transform.position.z),transform.rotation)as GameObject; // make a clone 1/2 a unit to the right in boss death

                clone1.transform.localScale = new Vector3(transform.localScale.y * 0.5f, transform.localScale.y * 0.5f, transform.localScale.z); // shrink the mini boss size by half each time in x and y
                clone1.GetComponent<TheBossHealthManager>().enemyHealth = 10; // mini boss health is 10
                clone2.transform.localScale = new Vector3(transform.localScale.y * 0.5f, transform.localScale.y * 0.5f, transform.localScale.z);// shrink the mini boss size by half each time in x and y
                clone2.GetComponent<TheBossHealthManager>().enemyHealth = 10; // mini boss health is 10

            }

            Destroy(gameObject); // destroy the killed bosses
        }


    }

    public void giveDamage(int damageToGive)
    {
        enemyHealth -= damageToGive; // this will take health away when damaged by player
        GetComponent<AudioSource>().Play(); // play the hit audio sound attached to boss
    }
}
