﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    public int healthToGive;
    public AudioSource pickUpSound;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<PlayerController>() == null)
            return;

        HealthManager.HurtPlayer(-healthToGive);
        pickUpSound.Play();
        Destroy(gameObject);
    }
}
