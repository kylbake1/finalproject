﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour
{
    public int enemyHealth; // creates an int called enemyHealth for the enemies
    public GameObject deathEffect; // creates a GameObject so we can insert the deatheffect into it
    public int pointsOnDeath; // points earned for killing enemy

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(enemyHealth <= 0) // if the enemys health is less then or eqaul to 0
        {
            Instantiate(deathEffect, transform.position, transform.rotation); // make a copy of the deatheffect, in the current position and rotation of parent
            ScoreManager.AddPoints(pointsOnDeath);//add the score 
            Destroy(gameObject);//destroy the enemy prefab
        }
    }

    public void giveDamage(int damageToGive) // this is the damage given from the player
    {
        enemyHealth -= damageToGive; // take the enemy health down by the value set by user
        GetComponent<AudioSource>().Play(); // play the audio atached to enemy when hit
    }
}
