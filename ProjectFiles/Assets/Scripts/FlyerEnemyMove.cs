﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyerEnemyMove : MonoBehaviour
{
    private PlayerController thePlayer; // creates a slot in inspector called thePlayer for the playerController
    public float moveSpeed; // creates a float to adjust the speed
    public float playerRange; // creates a float to adjust the playerange for enemy sight
    public LayerMask playerLayer; // creates a layermast called playerlayer
    public bool playerInRange; // creates a bool called playerInRange 
    public bool facingAway; // creates a bool called facingAway for following
    public bool followOnLookAway; // creates a bool called followonlookaway so enemy can chase player like ghosts in mario

    // Start is called before the first frame update
    void Start()
    {
        thePlayer = FindObjectOfType<PlayerController>(); // this will automatically set the playercontoller in thePlayer slot
    }

    // Update is called once per frame
    void Update()
    {
        playerInRange = Physics2D.OverlapCircle(transform.position, playerRange, playerLayer); // this will allow the enemy to move towards the player when were in range

        if (!followOnLookAway) // if followonlookaway is true
        {
            if (playerInRange) // if player is in range
            {
                transform.position = Vector3.MoveTowards(transform.position, thePlayer.transform.position, moveSpeed * Time.deltaTime); //move towards the player relative to time
                return; // return nothing if leave the range
            }
        }

        if((thePlayer.transform.position.x < transform.position.x && thePlayer.transform.localScale.x < 0) || (thePlayer.transform.position.x > transform.position.x && thePlayer.transform.localScale.x > 0)) // this will change the direction of the enemy based on the players looking direction if looking towards or looking away
        {
            facingAway = true; // looking away is true, chase
        }
        else
        {
            facingAway = false; // looking away is false, stop chase
        }

        if (playerInRange && facingAway) // if the player is in range and looking away
        {
            transform.position = Vector3.MoveTowards(transform.position, thePlayer.transform.position, moveSpeed * Time.deltaTime); // begin to move towards the player while conditons are true
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.position, playerRange); // this will display a sphere for the enemy so we can see the radius that needs to be crossed to begin chase
    }
}
