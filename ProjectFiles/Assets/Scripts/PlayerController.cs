﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb2d; //creates a slop for rigidbody2D called rb2d
    private float moveVelocity;// moveVelocity that is altered in script
    public float moveSpeed;// movespeed that is adjustable
    public float jumpHeight; //jumpheight that is adjustable

    public Transform groundCheck; // transform that is called groundcheck for player
    public float groundCheckRadius; // allows player to stand on ground below feel
    public LayerMask whatIsGround; // layer that attached to ground for player
    private bool grounded; // bool called grounded
    private bool doubleJumped; // bool called doublejump

    private Animator anim; // creates a slot called anim for animator

    public Transform firePoint; // emptygameobject called firepoint
    public GameObject ninjaStar;// gameobject slot for ninjastar
    public float shotDelay;// shotdelay that is adjustable
    private float shotDelayCounter; //shotdelaycounter that is adjustable

    public float knockBack; //knockback that is adjustable
    public float knockBackLength;//knockbacklength that is adjustable
    public float knockBackCount;//knockbackcount that is adjustable
    public bool knockFromRight;//bool

    public bool onLadder; // bool for climbing ladders
    public float climbSpeed; // how fast ladder climb
    private float climbVelocity; // climbing velocity
    private float gravityStore; // adjustbale gravity for climbing 


    // Start is called once we start the game
    void Start()
    {
        anim = GetComponent<Animator>(); //automatically fills anim with animator
        rb2d = GetComponent<Rigidbody2D>();// automatically fills rb2b with rigidbody2D
        gravityStore = rb2d.gravityScale;// automatically fills gravitystore with rigidbody2d gravity
    }

    private void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround); // player is grounded with several tools
    }

    // Update is called once per frame 60fps
    void Update()
    {
        if (grounded) // if the player is not grounded
            doubleJumped = false; // no double jumping
            anim.SetBool("Grounded", grounded); // animation for grounded starts
        
        if (Input.GetButtonDown("Jump") && grounded) // if user pressed the space key and is grounded
        {
            Jump(); // player can jump
        }

        if (Input.GetButtonDown("Jump") && !doubleJumped && !grounded) // if the user presses the space key and is not grounded
        {
            Jump(); //can jump
            doubleJumped = true; // and allowed another extra jump
        }


        moveVelocity = moveSpeed * Input.GetAxisRaw("Horizontal"); // if the user presses the AD or <> keys

        if (knockBackCount <= 0) // if the user knockback is less than or equal to 0
        {
            rb2d.velocity = new Vector2(moveVelocity, rb2d.velocity.y); // move the player in that direction
        }
        else
        {
            if (knockFromRight) // move the player to the left
                rb2d.velocity = new Vector2(-knockBack, knockBack); // applied velocity to player to throw
            if(!knockFromRight) // move the player to the right
                rb2d.velocity = new Vector2(knockBack, knockBack);// applied velocity to player to throw
            knockBackCount -= Time.deltaTime;// knockback in the air is reduced by time
        }

        anim.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x)); // sets the velocity of x to absolute for always positive

        if(rb2d.velocity.x > 0) // if velocity x is less than 0
                    transform.localScale = new Vector3(1f, 1f, 1f); // rotate the player left

        else if(rb2d.velocity.x < 0) //if velocity is greater than 0
            transform.localScale = new Vector3(-1f, 1f, 1f); // rotate the player right

        if (Input.GetButtonDown("Fire1")) // if the player presses the enter key
        {
            Instantiate(ninjaStar, firePoint.position, firePoint.rotation); // shoot ninja stars
            shotDelayCounter = shotDelay; // cool down time per shot
        }

        if (Input.GetButtonDown("Fire1")) // if the user presses the mouse button
        {
            shotDelayCounter -= Time.deltaTime; // shot cooldown is reduced

            if(shotDelayCounter <= 0)//if cooldown is less than or equal to 0
            {
                shotDelayCounter = shotDelay; // shoot
                Instantiate(ninjaStar, firePoint.position, firePoint.rotation); // close the ninja star
            }
        }

        if (anim.GetBool("Sword")) // animation for sword is played
            anim.SetBool("Sword", false); // animation for sword is not played

        if (Input.GetButtonDown("Fire2")) // if the L key is pressed
        {
            anim.SetBool("Sword", true); // swowd will comeout

        }

        if (onLadder)
        {
            rb2d.gravityScale = 0f;
            climbVelocity = climbSpeed * Input.GetAxisRaw("Vertical");
            rb2d.velocity = new Vector2(rb2d.velocity.x, climbVelocity);
        }

        if (!onLadder)
        {
            rb2d.gravityScale = gravityStore;
        }
    }
    
    public void Jump()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, jumpHeight);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "MovingPlatform")
        {
            transform.parent = collision.transform;
        }
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
    }
}
