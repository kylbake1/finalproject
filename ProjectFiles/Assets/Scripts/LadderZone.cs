﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderZone : MonoBehaviour
{
    private PlayerController thePlayer; // playercontroller is called thePlayer
    // Start is called before the first frame update
    void Start()
    {
        thePlayer = FindObjectOfType<PlayerController>(); // automatically set the playercontroller to thePlayer for us
    }

    void OnTriggerEnter2D(Collider2D collision) // if the collider is triggered
    { 
        if(collision.name == "Player") // only triggered by the Player
        {
            thePlayer.onLadder = true; // the player can now climb the ladder once in the collider
        }    
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Player") // only triggered by the player
        { 
            thePlayer.onLadder = false; // if the player leaves the collider, cant climb ladder
        }
    }
}
