﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    public float startingTime; // player set number of time for the game
    private Text theText; // the text is set to Text
    private PauseMenu thePauseMenu; // thepausemenu is set to the pausemenu script
    public PlayerController player;// player is set to the playercontroller script
    private float countingTime; // float for counting time
    private HealthManager theHealth; // healthmanager is now called thehealth

    // Start is called before the first frame update
    void Start()
    {
        countingTime = startingTime; // counting time is now set to user set time
        theText = GetComponent<Text>(); // thetext is automatically set to Text
        thePauseMenu = FindObjectOfType<PauseMenu>();// thePauseMenu is automatically filled with PauseMenu script
        theHealth = FindObjectOfType<HealthManager>(); //theHealth is automatically filled with the HealthManager script
    }

    // Update is called once per frame
    void Update()
    {
        if (thePauseMenu.isPaused) // if the game is paused
            return;

        countingTime -= Time.deltaTime; // reduce the time relative to gametime

        if(countingTime <= 0) // if the time is less than or equal to 0
        {
            theHealth.KillPlayer(); // player is killed

        }

        theText.text = "" + Mathf.Round(countingTime); // time is rounded from a float to a int
    }
    
    public void ResetTime()
    {
        countingTime = startingTime; //  time is reset back to player set time
    }

}
