﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LifeManager : MonoBehaviour
{
    private int lifeCounter; // life counter that is private
    private Text theText; // theText that is private
    public GameObject gameOverScreen; // slot for gameOverScreen is created
    public PlayerController player; // playercontroller slot created called player
    public string mainMenu; // scene name to input in mainMenu
    public float waitAfterGameOver; // time to see gameover screen

    // Start is called before the first frame update
    void Start()
    {
        theText = GetComponent<Text>(); //automatically fills theText with Text

        lifeCounter = PlayerPrefs.GetInt("PlayerCurrentLives"); //lifecounter is updated from playercurrenthealth

        player = FindObjectOfType <PlayerController> (); // automatically fills playercontroller in player
    }

    // Update is called once per frame
    void Update()
    {
        if(lifeCounter == 0) // if life is 0
        {
            gameOverScreen.SetActive(true); // gameover screen shows
            player.gameObject.SetActive(false); // player is turned off
        }
        theText.text = "x " + lifeCounter; // life counter is updated 

        if (gameOverScreen.activeSelf)
        {
            waitAfterGameOver -= Time.deltaTime; // gameover screen is shown for number of set time
        }

        if(waitAfterGameOver < 0) // if gameover screen is less than 0
        {
            SceneManager.LoadScene(mainMenu); //open the mainMenu scene
        }
    }

    public void GiveLife()
    {
        lifeCounter++; // life for player increases by one when heart is collected
        PlayerPrefs.SetInt ("PlayerCurrentLives", lifeCounter); // updated for other scenes
    }

    public void TakeLife()
    {
        lifeCounter--;// life for player decreased when killed
        PlayerPrefs.SetInt("PlayerCurrentLives", lifeCounter); // updated for other scenes
    }
}
