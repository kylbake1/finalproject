﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public static int playerHealth; // playerHealth that can be accessed in other scripts
    public int maxPlayerHealth;// maxplayerhealth that is adjustable
    public bool isDead; // bool called isdead

    public Slider healthBar; // slided called healthbar

    private LevelManager levelManager; // levelmanager called levelmanager
    private LifeManager lifeSystem; // lifesystem script called lifesystem

    private TimeManager theTime; //timemanager script called thetime

    // Start is called before the first frame update
    void Start()
    {
        healthBar = GetComponent<Slider>(); // automatically fill  healthBar with Slider
        playerHealth = PlayerPrefs.GetInt("PlayerCurrentHealth");// automatically fill playerHealth with Playercurrenthealth
        theTime = FindObjectOfType<TimeManager>();// automatically fill theTime with Timemanager
        levelManager = FindObjectOfType<LevelManager>();// automatically fill levelManager with LevelManager
        lifeSystem = FindObjectOfType<LifeManager>();// automatically fill lifeSystem with LifeManager
        isDead = false; // isdead is set to false
    }

    // Update is called once per frame
    void Update()
    {
        if(playerHealth <= 0 && !isDead) // if the player health is less than or equal to 0 and the is dead
        {
            playerHealth = 0; // playerhealth is set to 0
            levelManager.RespawnPlayer(); // player will respawn
            lifeSystem.TakeLife(); // player has lost a life
            isDead = true; // the player is dead
            theTime.ResetTime(); // the time resets back to use set time
        }

        if (playerHealth > maxPlayerHealth)// this will keep the playerhealth from passing max number set
            playerHealth = maxPlayerHealth;

        healthBar.value = playerHealth; //  healthbar is set to playerhealth 
    }

    public static void HurtPlayer(int damageToGive)//function that can be accessed in all scripts
    {
        playerHealth -= damageToGive; // player loses health when damaged
        PlayerPrefs.SetInt("PlayerCurrentHealth", playerHealth);  // transfered health to other scenes
    }

    public void fullHealth() 
    {
        playerHealth = PlayerPrefs.GetInt("PlayerMaxHealth"); // transferes playerhealth to other scenes
        PlayerPrefs.SetInt("PlayerCurrentHealth", playerHealth); // transferes playerscurrenthealth if damaged to other scenes
    }

    public void KillPlayer()
    {
        playerHealth = 0; // player is dead

    }
}
