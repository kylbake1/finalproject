﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStarController : MonoBehaviour
{
    public float speed; // speed for player adjusting
    public PlayerController player; // playercontroller slot called player
    public GameObject impactEffect; // impacteffect gameobject slot created
    public float rotationSpeed; // adjustable rotation speed
    public int damageToGive; // damages int that can be changed
    private Rigidbody2D rb2d; // rigidbody slot called rb2d

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>(); // automatically fills playercontroller in player
        rb2d = GetComponent<Rigidbody2D>(); // automatically fills rigidbody2d for rb2d

        if (player.transform.position.x < transform.position.x)// if the player position in less than enemy sight
        {
            speed = -speed; // star will move left or right
            rotationSpeed = -rotationSpeed; // star will spin 360 or -360
        }
    }

    // Update is called once per frame
    void Update()
    {
        rb2d.velocity = new Vector2(speed, rb2d.velocity.y); // allows the star to move
        rb2d.angularVelocity = rotationSpeed; // allows the star to rotate
    }

    private void OnTriggerEnter2D(Collider2D collision) // collider triggered
    {
        if (collision.name == "Player") // triggered only by player
        {
            HealthManager.HurtPlayer(damageToGive); // hurt the player by set int of damage
        }
        Instantiate(impactEffect, transform.position, transform.rotation); // create the impact effect
        Destroy(gameObject);//destroy the star
    }
}
