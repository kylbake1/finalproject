﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayerOnContact : MonoBehaviour
{
    public int damageToGive; // number of damage points dealt to player

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D collision) // if something triggers the collider
    {
        if (collision.name == "Player") // collider name is on player
        {
            HealthManager.HurtPlayer(damageToGive); // reduce the health of player by the damage points of enemy
            collision.GetComponent<AudioSource>().Play(); // play the damage sound

            var player = collision.GetComponent<PlayerController>(); // 
            player.knockBackCount = player.knockBackLength;// knock the player back when hit

            if (collision.transform.position.x < transform.position.x) // if the player is hit
                player.knockFromRight = true;// move them left
            else
                player.knockFromRight = false; //move them right
        }
    }
}
