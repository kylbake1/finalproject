﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickup : MonoBehaviour
{
    public int pointsToAdd; // int for points to add when we collect coins
    public AudioSource coinSoundEffect; // sound for coin pickup

    private void OnTriggerEnter2D(Collider2D collision) // if we enter the collider
    {
        if (collision.GetComponent<PlayerController>() == null) // nothing else but the player can enter the collider
            return; //nothing happens if it aint the player

        ScoreManager.AddPoints(pointsToAdd); // add the points set by user
        coinSoundEffect.Play();// plays the coin sound in audiosource 
        Destroy(gameObject); // destroys the coin
    }

}
