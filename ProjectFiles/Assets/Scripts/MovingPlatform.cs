﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public GameObject platform; // creates a slot to insert the platform gameobject
    public float moveSpeed; // speed at which the platform moves
    public Transform currentPoint; // point A and point B
    public Transform[] points; // array of all possible points
    public int pointSelection; // alternates from point a and b

    // Start is called before the first frame update
    void Start()
    {
        currentPoint = points[pointSelection]; // current point is set equal to the point selected
    }

    // Update is called once per frame
    void Update()
    {
        platform.transform.position = Vector3.MoveTowards(platform.transform.position, currentPoint.position, Time.deltaTime * moveSpeed); // moves the platform 
        if(platform.transform.position == currentPoint.position)//if the platform is equal to the current point
        {
            pointSelection++;//go to the next point in the array
            if(pointSelection == points.Length)// pointselection is set to the lenght of points to not go over
            {
                pointSelection = 0;// pointselection is 0  
            }
            currentPoint = points[pointSelection];
        }
    }
}
