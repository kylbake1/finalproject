﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectManager : MonoBehaviour
{
    public string[] levelTags; // creates an array for us called levelTags
    public GameObject[] locks; // creates a gameobject array called locks that are in the scene
    public bool[] levelUnlocked; // creates a bool called levelunlocked to keep doors locked
    

    void Start()
    {
        for (int i = 0; i < levelTags.Length; i++) // this will work through out arrays for us
        {
            if (PlayerPrefs.GetInt(levelTags[i]) == null) // if the door is locked
            {
                levelUnlocked[i] = false; //keep door locked
            }
            else if (PlayerPrefs.GetInt(levelTags[i]) == 0) // if the door isnt unlocked and player hasnt opened doors
            {
                levelUnlocked[i] = false; // keep doors locked
            }
            else // if player beats a level and enters a door
            {
                levelUnlocked[i] = true; // unlock the next doos
            }

            if (levelUnlocked[i]) // keeps track of open doors in bool
            {
                locks[i].SetActive(false); // puts a mark next to open doors
            }

        }
    }

    void Update()
    {

    }
}

