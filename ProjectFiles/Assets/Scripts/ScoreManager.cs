﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static int score; // creates a int called score than can be accessed anywhere in other scripts

    Text text; // UI text called text

    void Start()
    {
        text = GetComponent<Text>(); // Text is automaticalled called text
        score = PlayerPrefs.GetInt("CurrentScore");// score is transferable from scene to scene
    }

    void Update()
    {
        if (score < 0) // if the score is less than 0
            score = 0;// score is equal to 0

        text.text = "" + score;        // score is updated with current score
    }

    public static void AddPoints(int pointsToAdd) //  this is accessable anywhere in other scripts to add points for the player 
    {
        score += pointsToAdd; // points are added to the current points value
        PlayerPrefs.SetInt("CurrentScore",score); // points are saved
    }

    public static void Reset()
    {
        score = 0; // score is set to 0 once restarted
        PlayerPrefs.SetInt("CurrentScore", score); // points are updated with the new 0 value
    }
}
